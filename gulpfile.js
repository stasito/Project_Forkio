const gulp = require('gulp');
const sass = require('gulp-sass');
sass.compiler = require('node-sass');
const browserSync = require('browser-sync').create();
const concat = require('gulp-concat');
const rigger = require('gulp-rigger');


function style() {
	return gulp.src('./scss/**/*.scss').
	pipe(sass()).on('error', sass.logError).
	           pipe(gulp.dest('./css')).
	           pipe(browserSync.stream());
}

function concatHTML () {
	return gulp.src('./src/html/index.html')
	           .pipe(rigger())
	           .pipe(gulp.dest('./dist/'));
}

// function rigerHTML() {
// 	gulp.src('app/*.js')
// 	    .pipe(rigger())
// 	    .pipe(gulp.dest('build/'));
// }

function watch() {
	browserSync.init({
		server: {
			baseDir: './'
		}
	});
	gulp.watch('./scss/**/*.scss', style);
	gulp.watch('./*.html').on('change', browserSync.reload);
	gulp.watch('./script/**/*.js').on('change', browserSync.reload);
}

gulp.task('riggerHTML', concatHTML);

exports.style = style;
exports.watch = watch;
